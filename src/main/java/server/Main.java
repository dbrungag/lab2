package server;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if (args.length != 1){
            System.out.println("Usage: programm port");
            return;
        }
        try {
            Server server = new Server(Integer.parseInt(args[0]));
            server.run();
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
