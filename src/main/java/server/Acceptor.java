package server;

import java.io.*;
import java.net.Socket;
import java.util.HashSet;

public class Acceptor implements Runnable {
    private Socket socket;
    private HashSet<User> users;
    private File dir;

    public Acceptor(Socket pSocket, HashSet<User> usList, File pDir) {
        users = usList;
        socket = pSocket;
        dir = pDir;
    }

    public void run() {
        User user = null;
        try {
            user = new User(socket, dir);
            synchronized (users){
                users.add(user);
            }
        } catch (IOException exc) {
            exc.printStackTrace();
            synchronized (users){
                users.remove(user);
            }
        }
        return;
    }
}
