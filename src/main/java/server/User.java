package server;

import java.io.*;
import java.net.Socket;
import java.time.Instant;

public class User {
    private Socket socket;
    private File file;
    private Long fSize;
    private FileOutputStream fileStr;
    private DataInputStream inStr;
    private DataOutputStream outStr;
    private int state;
    private long time;
    private long startTime;
    private long blocksRecv;
    private long blocksLast;


    public User(Socket uSocket, File dir) throws IOException {
        String fName;
        socket = uSocket;
        inStr = new DataInputStream(socket.getInputStream());
        outStr = new DataOutputStream(socket.getOutputStream());
        fSize = inStr.readLong();
        fName = inStr.readUTF();
        file = new File(dir, fName);
        fileStr = new FileOutputStream(file);
        state = 0;
        startTime = Instant.now().getEpochSecond();
        time = startTime;
        blocksRecv = 0;
        blocksLast = 0;
    }
    public File getFile() {
        return file;
    }
    public Long getfSize() {
        return fSize;
    }
    public FileOutputStream getFileStr() {
        return fileStr;
    }
    public DataInputStream getInStr() {
        return inStr;
    }
    public DataOutputStream getOutStr() {
        return outStr;
    }
    public int state() {
        return state;
    }
    public void setReady() {
        state = 0;
    }
    public void setBusy() {
        state = 1;
    }
    public void setComplete() throws IOException {
        state = 2;
        fileStr.close();
        inStr.close();
        outStr.close();
        socket.close();
    }
    public void errReact() throws IOException {
        state = 2;
        fileStr.close();
        inStr.close();
        outStr.close();
        socket.close();
        file.delete();
    }
    public long tStart() {
        return startTime;
    }
    public void renewTime() {
        time = Instant.now().getEpochSecond();
    }
    public long getTime() {
        return time;
    }
    public void markBlock() {
        blocksRecv++;
    }
    public long getBlocksRecv() {
        return blocksRecv;
    }
    public long getBlocksLast() {
        return blocksLast;
    }
    public void setBlocksLast(long val) {
        blocksLast = val;
    }
}
