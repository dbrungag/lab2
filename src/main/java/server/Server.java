package server;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private ServerSocket socket;
    private ExecutorService pool;
    private HashSet<User> users;

    public Server(int portNmb) throws IOException {
        socket = new ServerSocket(portNmb);
        pool = Executors.newFixedThreadPool(6);
        users = new HashSet();
    }

    public void run() {
        try{
            File file = new File("uploads");
            Thread thread = new Thread(new Receiver(users, pool));
            thread.start();
            file.mkdir();
            while (true)
            {
                Socket clSocket = socket.accept();
                pool.execute(new Acceptor(clSocket, users, file));
            }
        } catch (IOException exc) {
            pool.shutdown();
            exc.printStackTrace();
        }
    }
}
