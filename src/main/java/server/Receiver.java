package server;

import java.util.HashSet;
import java.util.concurrent.ExecutorService;

public class Receiver implements Runnable {
    private ExecutorService pool;
    private HashSet<User> users;
    private int ready = 0;
    private int toDelete = 2;

    public Receiver(HashSet<User> usList, ExecutorService tPool) {
        users = usList;
        pool = tPool;
    }

    public void run () {
        while (true) {
            synchronized (users) {
                for (User user : users) {
                    if (user.state() == ready) {
                        user.setBusy();
                        pool.execute(new Handler(user));
                    }
                }
                users.removeIf(user -> user.state() == toDelete);
            }
        }
    }
}

