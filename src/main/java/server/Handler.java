package server;

import java.io.*;
import java.time.Instant;

public class Handler implements Runnable {
    private User user;
    private String scsMsg = "Операция прошла успешно.";
    private String errMsg = "Операция провалилась.";
    private String instSpeed = "Мгновенная скорость: ";
    private String avgSpeed = "Средняя скорость за сеанс: ";
    private String uofMeas = "КБ/с";
    private int blockSize = 1024;
    private int sndGap = 3;

    public Handler(User currUser) {
        user = currUser;
    }

    public void run() {
        DataInputStream inStr = user.getInStr();
        FileOutputStream fileStr = user.getFileStr();
        DataOutputStream outStr = user.getOutStr();
        byte[] buf = new byte[blockSize];
        try {
            int ret = inStr.read(buf);
            fileStr.write(buf, 0, ret);
            if (ret < blockSize){
                File file = user.getFile();
                long fSize = file.length(),
                        origSize = user.getfSize().longValue(),
                        gap = Instant.now().getEpochSecond() - user.tStart();
                double recvdNow = user.getBlocksRecv() + (double) ret / blockSize, tSpeed;
                if (gap == 0) {
                    gap++;
                }
                tSpeed = recvdNow / gap;
                outStr.writeUTF(instSpeed + new Double(tSpeed).toString() + uofMeas);
                outStr.writeUTF(avgSpeed + new Double(tSpeed).toString() + uofMeas);
                if (fSize == origSize) {
                    outStr.writeUTF(scsMsg);
                }
                else {
                    outStr.writeUTF(errMsg);
                }
                user.setComplete();
            }
            else {
                long curTime = Instant.now().getEpochSecond(), gap;
                gap = curTime - user.getTime();
                user.markBlock();
                if (gap >= sndGap) {
                    double recvdNow = user.getBlocksRecv(),
                            tInst = (recvdNow - user.getBlocksLast()) / gap,
                            tAvg = recvdNow / (curTime - user.tStart());
                    outStr.writeUTF(instSpeed + new Double(tInst).toString() + uofMeas);
                    outStr.writeUTF(avgSpeed + new Double(tAvg).toString() + uofMeas);
                    user.setBlocksLast(user.getBlocksRecv());
                    user.renewTime();
                }
                user.setReady();
            }
        }
        catch (IOException exc) {
            try {
                user.errReact();
            } catch (IOException e) {
                e.printStackTrace();
            }
            exc.printStackTrace();
        }
    }
}
