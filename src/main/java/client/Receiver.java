package client;

import java.io.DataInputStream;
import java.io.IOException;

public class Receiver implements Runnable {
    private DataInputStream inStr;
    public Receiver(DataInputStream str){
        inStr = str;
    }

    public void run () {
        try {
            while (true) {
                String tString = inStr.readUTF();
                System.out.println(tString);
                if (tString.endsWith(".")) {
                    break;
                }

            }
        } catch (IOException exc) {
            exc.printStackTrace();
        }
    }
}
