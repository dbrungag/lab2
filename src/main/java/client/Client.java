package client;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;

public class Client {
    private Socket socket;
    private File file;
    private int blockSize = 1024;

    public Client(String filepath, String ip, Integer portNmb) throws IOException {
        socket = new Socket(InetAddress.getByName(ip), portNmb);
        file = new File(filepath);
    }

    public void run() throws IOException, InterruptedException {
        byte[] buf = new byte[blockSize];
        FileInputStream fileStr = new FileInputStream(file);
        DataInputStream inStr = new DataInputStream(socket.getInputStream());
        DataOutputStream outStr = new DataOutputStream(socket.getOutputStream());
        Receiver receiver = new Receiver(inStr);
        Thread thread = new Thread(receiver);
        thread.start();
        outStr.writeLong(file.length());
        outStr.writeUTF(file.getName());
        while (true) {
            int ret = fileStr.read(buf);
            if (ret == -1) {
                break;
            }
            outStr.write(buf, 0, ret);
        }
        thread.join();
        return;
    }
}
