package client;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
        if (args.length != 3){
            System.out.println("Usage: programm filepath ip port");
            return;
        }
        try {
            Client client = new Client(args[0], args[1], Integer.parseInt(args[2]));
            client.run();
        } catch (IOException exc) {
            exc.printStackTrace();
        } catch (InterruptedException exc) {
            exc.printStackTrace();
        }
    }
}
